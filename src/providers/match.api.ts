import { Injectable } from '@angular/core';
import { LoadingController, Loading} from 'ionic-angular';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Headers, RequestOptions } from '@angular/http';
import { ApiConfig,DBHelper } from '../app/api.config'

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

/*
  Generated class for the Test provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class MatchApi {

    constructor(public http: Http) {

    }

    
//获取比赛列表，传入对应的搜索条件
public list(search_condition_json, showLoadingModal:boolean=true) {
        var url = ApiConfig.getApiUrl()+"match/list";
        var headers = ApiConfig.GetHeader(url, search_condition_json);
        let options = new RequestOptions({ headers: headers });
        let body=ApiConfig.ParamUrlencoded(search_condition_json);


        let loading: Loading=null;
        if(showLoadingModal){
          loading = ApiConfig.GetLoadingModal();
        }

        return this.http.post(url, body, options).toPromise()
            .then((res) => {
                ApiConfig.DimissLoadingModal(loading);
                return res.json();
            })
            .catch(err => {
                ApiConfig.DimissLoadingModal(loading);
                this.handleError(err);
            });

        
    }


//先从服务器中同步数据到本地数据库，再从本地数据库中搜索数据
public listInDB(search_condition_json, callback, showLoadingModal:boolean=true) {
    
    var db = DBHelper.GetInstance();
    if (db.isDBReady() == false) {
        this.list(search_condition_json).then(data => {
            callback(data);
        });
        return;
    }
        var ost = this;
        this.createTable();

        let loading: Loading=null;
        if(showLoadingModal){
          loading = ApiConfig.GetLoadingModal();
        }

        try {
            db.getLastestUpdatedTime(this.tableName(), function (calltime) {
                //alert(calltime);
                var url = ApiConfig.getApiUrl() + "match/list";
                var headers = ApiConfig.GetHeader(url, {"lastupdatecalltime":calltime});
                let options = new RequestOptions({ headers: headers });
                let body = ApiConfig.ParamUrlencoded({"lastupdatecalltime":calltime});
                try {
                    var ret = ost.http.post(url, body, options).toPromise()
                        .then(res => {
                            var data = res.json();
                            try {
                                DBHelper.GetInstance().batchUpdate(ost.tableName(), ost.tableColumns(), data, function () {
                                        ApiConfig.DimissLoadingModal(loading);
                                    DBHelper.GetInstance().updateLastestCallTime(ost.tableName());
                                    ost.query(search_condition_json, callback);
                                });
                            } catch (ex) {
                                ApiConfig.DimissLoadingModal(loading);
                                ost.query(search_condition_json, callback);
                            }
                        })
                        .catch(err => {
                            ApiConfig.DimissLoadingModal(loading);
                            ost.query(search_condition_json, callback);
                        });
                } catch (err){
                    ApiConfig.DimissLoadingModal(loading);
                }
            });
        } catch (ex){
            ApiConfig.DimissLoadingModal(loading);
            this.query(search_condition_json, callback);
        }
}

    public query(condition, callback) {
		
		var db = DBHelper.GetInstance();
		if (db.isDBReady() == false) {
			this.list(condition).then(data => {
				callback(data);
			});
			return;
		}
		
		
        this.createTable();
        var sql = "select * from " + this.tableName() + " where 1=1 ";
        var searchdata = new Array();
        for (let i in condition) {
            var columns = this.tableColumns();
            var coltype = columns[i];
            if (coltype == "varchar") {
                sql += " and " + i + " like ?";
                searchdata.push(condition[i]);
            } else if (coltype=="int") {
                sql += " and " + i + " = ?";
                searchdata.push(condition[i]);
            }
        }
        if (condition["orderby"] != null && condition["orderby"] != "") {
            sql += " order by "+condition["orderby"];
        }

        DBHelper.GetInstance().query(sql, searchdata, callback);
    }

    public tableName() {
        return "match";
    }

    public static CreateTable = false;
    public createTable() {
        if (MatchApi.CreateTable == false) {
            DBHelper.GetInstance().createTable(this.tableName(), this.tableColumns());
            MatchApi.CreateTable = true;
        }
    }

    public tableColumns() {
        var columns = {};
    columns["name"] = "varchar";//名称
    columns["order_date"] = "varchar";//预约时间
    columns["status"] = "varchar";//状态
    columns["status_name"] = "varchar";//状态
    columns["game_type"] = "varchar";//比赛类型
    columns["game_type_name"] = "varchar";//比赛类型
    columns["create_by"] = "int";//创建者
    columns["create_by_name"] = "varchar";//创建者
    columns["game_status"] = "varchar";//比赛状态
    columns["game_status_name"] = "varchar";//比赛状态
    columns["create_date"] = "varchar";//创建时间
    columns["end_date"] = "varchar";//完成时间
    columns["game_result"] = "varchar";//比赛结果
    columns["game_round"] = "int";//比赛局数
    columns["round_result"] = "varchar";//每局结果
        return columns;
    }





//获取比赛详情, 传入对应的id
public get(id, showLoadingModal:boolean=true) {
        var url = ApiConfig.getApiUrl()+'match/get';
        let json={ 'id' : id };
        var headers = ApiConfig.GetHeader(url, json);
        let options = new RequestOptions({ headers: headers });
        let body=ApiConfig.ParamUrlencoded(json);


        let loading: Loading=null;
        if(showLoadingModal){
          loading = ApiConfig.GetLoadingModal();
        }

        return this.http.post(url, body, options).toPromise()
            .then((res) => {
                ApiConfig.DimissLoadingModal(loading);
                return res.json();
            })
            .catch(err => {
                ApiConfig.DimissLoadingModal(loading);
                this.handleError(err);
            });

        
    }


//更新比赛，传入对应的表字段
public update(update_json, showLoadingModal:boolean=true) {
        var url = ApiConfig.getApiUrl()+'match/update';
        var headers = ApiConfig.GetHeader(url, update_json);
        let options = new RequestOptions({ headers: headers });
        let body=ApiConfig.ParamUrlencoded(update_json);


        let loading: Loading=null;
        if(showLoadingModal){
          loading = ApiConfig.GetLoadingModal();
        }

        return this.http.post(url, body, options).toPromise()
            .then((res) => {
                ApiConfig.DimissLoadingModal(loading);
                return res.json();
            })
            .catch(err => {
                ApiConfig.DimissLoadingModal(loading);
                this.handleError(err);
            });

        
    }


//
public setfinished(data, showLoadingModal:boolean=true) {
        var url = ApiConfig.getApiUrl()+'match/setfinished';
        var headers = ApiConfig.GetHeader(url, data);
        let options = new RequestOptions({ headers: headers });

        let body=ApiConfig.ParamUrlencoded(data);

        let loading: Loading=null;
        if(showLoadingModal){
          loading = ApiConfig.GetLoadingModal();
        }

        return this.http.post(url, body, options).toPromise()
            .then((res) => {
                ApiConfig.DimissLoadingModal(loading);
                return res.json();
            })
            .catch(err => {
                ApiConfig.DimissLoadingModal(loading);
                this.handleError(err);
            });

        
    }


//
public setremoved(data, showLoadingModal:boolean=true) {
        var url = ApiConfig.getApiUrl()+'match/setremoved';
        var headers = ApiConfig.GetHeader(url, data);
        let options = new RequestOptions({ headers: headers });

        let body=ApiConfig.ParamUrlencoded(data);

        let loading: Loading=null;
        if(showLoadingModal){
          loading = ApiConfig.GetLoadingModal();
        }

        return this.http.post(url, body, options).toPromise()
            .then((res) => {
                ApiConfig.DimissLoadingModal(loading);
                return res.json();
            })
            .catch(err => {
                ApiConfig.DimissLoadingModal(loading);
                this.handleError(err);
            });

        
    }


//
public updateresult(data, showLoadingModal:boolean=true) {
        var url = ApiConfig.getApiUrl()+'match/updateresult';
        var headers = ApiConfig.GetHeader(url, data);
        let options = new RequestOptions({ headers: headers });

        let body=ApiConfig.ParamUrlencoded(data);

        let loading: Loading=null;
        if(showLoadingModal){
          loading = ApiConfig.GetLoadingModal();
        }

        return this.http.post(url, body, options).toPromise()
            .then((res) => {
                ApiConfig.DimissLoadingModal(loading);
                return res.json();
            })
            .catch(err => {
                ApiConfig.DimissLoadingModal(loading);
                this.handleError(err);
            });

        
    }



    private handleError(error: Response) {
        console.log(error);
        return Observable.throw(error.json().error || 'Server Error');
    }

}
