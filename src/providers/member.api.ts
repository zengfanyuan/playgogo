import { Injectable } from '@angular/core';
import { LoadingController, Loading} from 'ionic-angular';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Headers, RequestOptions } from '@angular/http';
import { ApiConfig,DBHelper } from '../app/api.config'

import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';

/*
  Generated class for the Test provider.

  See https://angular.io/docs/ts/latest/guide/dependency-injection.html
  for more info on providers and Angular 2 DI.
*/
@Injectable()
export class MemberApi {

    constructor(public http: Http) {

    }

    

//
public checkregister(data, showLoadingModal:boolean=true) {
        var url = ApiConfig.getApiUrl()+'member/checkregister';
        var headers = ApiConfig.GetHeader(url, data);
        let options = new RequestOptions({ headers: headers });

        let body=ApiConfig.ParamUrlencoded(data);

        let loading: Loading=null;
        if(showLoadingModal){
          loading = ApiConfig.GetLoadingModal();
        }

        return this.http.post(url, body, options).toPromise()
            .then((res) => {
                ApiConfig.DimissLoadingModal(loading);
                return res.json();
            })
            .catch(err => {
                ApiConfig.DimissLoadingModal(loading);
                this.handleError(err);
            });

        
    }


//
public info(data, showLoadingModal:boolean=true) {
        var url = ApiConfig.getApiUrl()+'member/info';
        var headers = ApiConfig.GetHeader(url, data);
        let options = new RequestOptions({ headers: headers });

        let body=ApiConfig.ParamUrlencoded(data);

        let loading: Loading=null;
        if(showLoadingModal){
          loading = ApiConfig.GetLoadingModal();
        }

        return this.http.post(url, body, options).toPromise()
            .then((res) => {
                ApiConfig.DimissLoadingModal(loading);
                return res.json();
            })
            .catch(err => {
                ApiConfig.DimissLoadingModal(loading);
                this.handleError(err);
            });

        
    }


//
public login(data, showLoadingModal:boolean=true) {
        var url = ApiConfig.getApiUrl()+'member/login';
        var headers = ApiConfig.GetHeader(url, data);
        let options = new RequestOptions({ headers: headers });

        let body=ApiConfig.ParamUrlencoded(data);

        let loading: Loading=null;
        if(showLoadingModal){
          loading = ApiConfig.GetLoadingModal();
        }

        return this.http.post(url, body, options).toPromise()
            .then((res) => {
                ApiConfig.DimissLoadingModal(loading);
                return res.json();
            })
            .catch(err => {
                ApiConfig.DimissLoadingModal(loading);
                this.handleError(err);
            });

        
    }


//
public updateinfo(data, showLoadingModal:boolean=true) {
        var url = ApiConfig.getApiUrl()+'member/updateinfo';
        var headers = ApiConfig.GetHeader(url, data);
        let options = new RequestOptions({ headers: headers });

        let body=ApiConfig.ParamUrlencoded(data);

        let loading: Loading=null;
        if(showLoadingModal){
          loading = ApiConfig.GetLoadingModal();
        }

        return this.http.post(url, body, options).toPromise()
            .then((res) => {
                ApiConfig.DimissLoadingModal(loading);
                return res.json();
            })
            .catch(err => {
                ApiConfig.DimissLoadingModal(loading);
                this.handleError(err);
            });

        
    }


//
public updatephoto(data, showLoadingModal:boolean=true) {
        var url = ApiConfig.getApiUrl()+'member/updatephoto';
        var headers = ApiConfig.GetHeader(url, data);
        let options = new RequestOptions({ headers: headers });

        let body=ApiConfig.ParamUrlencoded(data);

        let loading: Loading=null;
        if(showLoadingModal){
          loading = ApiConfig.GetLoadingModal();
        }

        return this.http.post(url, body, options).toPromise()
            .then((res) => {
                ApiConfig.DimissLoadingModal(loading);
                return res.json();
            })
            .catch(err => {
                ApiConfig.DimissLoadingModal(loading);
                this.handleError(err);
            });

        
    }



    private handleError(error: Response) {
        console.log(error);
        return Observable.throw(error.json().error || 'Server Error');
    }

}
