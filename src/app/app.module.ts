import { NgModule } from '@angular/core';
import { HttpModule, Http } from '@angular/http';
import { IonicApp, IonicModule } from 'ionic-angular';
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate';

import { MyApp } from './app.component';
import { CompetitionPage } from '../pages/competition/competition';
import { NewCompetitionPage } from '../pages/competition-new/competition-new';
import { CompetitionListPage } from '../pages/competition-list/competition-list';
import { CompetitionScoreboardPage } from '../pages/competition-scoreboard/competition-scoreboard';
import { CompetitionViewPage } from '../pages/competition-view/competition-view';
import { FriendsPage } from '../pages/friends/friends';
import { MePage } from '../pages/me/me';
import { MePhotoPage } from '../pages/me-photo/me-photo';
import { AboutUSPage } from '../pages/about-us/about-us';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { CompetitionRoundPage } from '../pages/competition-round/competition-round';
import { CompetitionSubRoundPage } from '../pages/competition-sub-round/competition-sub-round';

export function createTranslateLoader(http: Http) {
  return new TranslateStaticLoader(http, './assets/i18n', '.json');
}

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    CompetitionPage,
    NewCompetitionPage,
    CompetitionListPage,
    CompetitionScoreboardPage,
    CompetitionViewPage,
    CompetitionRoundPage,
    CompetitionSubRoundPage,
    FriendsPage,
    MePage,
    MePhotoPage,
    AboutUSPage,
    TabsPage
  ],
  imports: [
    HttpModule,
    TranslateModule.forRoot({
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [Http]
    }),
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    CompetitionPage,
    NewCompetitionPage,
    CompetitionListPage,
    CompetitionScoreboardPage,
    CompetitionRoundPage,
    CompetitionSubRoundPage,
    CompetitionViewPage,
    FriendsPage,
    MePage,
    MePhotoPage,
    AboutUSPage,
    TabsPage
  ],
  providers: []
})
export class AppModule {}
