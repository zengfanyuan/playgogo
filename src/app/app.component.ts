import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from 'ionic-native';
import { TranslateService } from 'ng2-translate';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Globalization } from '@ionic-native/globalization';

import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { ApiConfig } from '../app/api.config';


@Component({
  templateUrl: 'app.html',
  providers: [SplashScreen, Globalization]
})
export class MyApp {

  rootPage: any;


  constructor(platform: Platform, translate: TranslateService, splashScreen: SplashScreen, globalization: Globalization) {
    let self = this;
    platform.ready().then(() => {
      let storage = window.localStorage;
      let token = storage.getItem("token");
      let lang = storage.getItem("lang");

      if (token) {
        self.rootPage = TabsPage;
        let id = storage.getItem("id");
        ApiConfig.SetToken(token, id);
      }
      else {
        self.rootPage = LoginPage;
      }
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      StatusBar.styleDefault();

      lang = "zh";
      if (lang) {
        setTimeout(() => {
          splashScreen.hide();
        }, 100);
        // this language will be used as a fallback when a translation isn't found in the current language
        translate.setDefaultLang(lang);
        // the lang to use, if the lang isn't available, it will use the current loader to get them
        translate.use(lang);
      }
      else {
        globalization.getPreferredLanguage().then(res => {
          setTimeout(() => {
            splashScreen.hide();
          }, 100);
          let lang = res.value.split("-")[0];
          // this language will be used as a fallback when a translation isn't found in the current language
          translate.setDefaultLang(lang);
          // the lang to use, if the lang isn't available, it will use the current loader to get them
          translate.use(lang);
        });
      }
    });
  }

}
