import { Contacts, ContactFindOptions, ContactFieldType } from '@ionic-native/contacts';
import { Util } from '../common/util';
import { MemberApi } from '../providers/member.api';

export class MyContacts {
  public allContacts: any;
  public allContactsMobile = [];
  public friendContacts = [];
  public mobileNameDictionary = [];
  private storage = window.localStorage;


  constructor(public contacts: Contacts, public memberApi: MemberApi) {
    let fields: ContactFieldType[] = ['displayName', 'phoneNumbers'];
    let options = new ContactFindOptions();
    options.filter = "";
    options.multiple = true;
    this.contacts.find(fields, options).then(allDatas => {
      this.allContacts = allDatas;
      for (let data of allDatas) {
        if (data.phoneNumbers && data.phoneNumbers.length > 0) {
          let phoneNumber = data.phoneNumbers[0].value.replace(/[^0-9]/g, "");
          if (Util.IsMobile(phoneNumber) && phoneNumber != this.storage.getItem("mobile")) {
            this.allContactsMobile.push(phoneNumber);
            this.mobileNameDictionary[phoneNumber] = data.displayName;
          }
        }
      }
      var postData = { "list": this.allContactsMobile };
      this.memberApi.checkregister(postData).then(friendDatas => {
        for (let data of friendDatas) {
          this.friendContacts.push({ "mobile": data.mobile, "name": this.mobileNameDictionary[data.mobile] });
        }
        this.storage.removeItem("friends");
        if (this.friendContacts && this.friendContacts.length > 0) {
          this.storage.setItem("friends", JSON.stringify(this.friendContacts));
        }
      });

    });
  }
}