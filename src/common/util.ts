import * as moment from 'moment';
export class Util {

    public static IsMobile(str: string): boolean {
        let reg = /^1[3|4|5|7|8]\d{9}$/;
        if (reg.test(str)) {
            return true;
        }
        return false;
    }

    public static FormatDate(date: string): string {
        return moment(date).format("YYYY-MM-DD")
    }

    public static HtmlDecode(str) {
        var s = "";
        if (str.length == 0) return "";
        s = str.replace(/&amp;/g, "&");
        s = s.replace(/&lt;/g, "<");
        s = s.replace(/&gt;/g, ">");
        s = s.replace(/&nbsp;/g, " ");
        s = s.replace(/&#39;/g, "\'");
        s = s.replace(/&quot;/g, "\"");

        return s;
    }
}