import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MyContacts } from '../../common/myContacts';
import { Contacts } from '@ionic-native/contacts';
import { MemberApi } from '../../providers/member.api';
import { MatchApi } from '../../providers/match.api';
import * as moment from 'moment';

@Component({
    selector: 'page-competition-new',
    templateUrl: 'competition-new.html',
    providers: [Contacts, MemberApi, MatchApi]
})
export class NewCompetitionPage {
    friendContacts: any;
    storage = window.localStorage;
    selfChecked: boolean;
    checkCount: number;
    gameType: number = 3;
    gameRound: number = 3;
    username: string;
    orderDate: any;
    today: any;
    maxDate: any;

    constructor(public contacts: Contacts, public memberApi: MemberApi, public matchApi: MatchApi, public navCtrl: NavController) {
        if (!this.storage.getItem("friends")) {
            let myContacts = new MyContacts(this.contacts, this.memberApi);
            this.friendContacts = myContacts.friendContacts;
        }
        else {
            this.friendContacts = JSON.parse(this.storage.getItem("friends"));
        }
        this.username = this.storage.getItem("name") || this.storage.getItem("mobile");
        this.today = moment().format("YYYY-MM-DD");
        this.maxDate = moment().add(5, 'y').format("YYYY-MM-DD");
        this.orderDate = this.today;
    }

    createCompetition() {
        let gameName = "";
        let gameResult = [];
        if (this.selfChecked) {
            gameName = this.username + " VS ";
            gameResult.push({ mobile: this.storage.getItem("mobile"), name: this.username });
        }

        for (let friend of this.friendContacts) {
            if (friend.checked) {
                gameName ? gameName += friend.name : gameName = friend.name + " VS ";
                gameResult.push({ mobile: friend.mobile, name: friend.name });
            }
        }

        let postData = { name: gameName, order_date: this.orderDate, game_type: this.gameType, game_round: this.gameRound, game_status: 1, create_date: this.today, end_date: null, game_result: JSON.stringify(gameResult) };
        this.matchApi.update(postData).then(response => {
            if (response.code == 0) {
                this.navCtrl.pop();
            }
        });
    }

    disableCheckbox(checked: boolean) {
        this.checkCount = this.selfChecked ? 1 : 0;
        for (let friend of this.friendContacts) {
            friend.checked && this.checkCount++;
        }
        if (this.checkCount >= 2 && !checked) {
            return true;
        }
        return false;
    }
    disableCreateButton() {
        return this.checkCount == 2 ? false : true;
    }

}
