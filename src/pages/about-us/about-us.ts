import { Component } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version';


@Component({
  selector: 'page-about-us',
  templateUrl: 'about-us.html',
  providers: [AppVersion]
})
export class AboutUSPage {

  appName: any;
  packageName: any;
  versionCode: any;
  versionNumber: any;

  constructor(private appVersion: AppVersion) {
    this.appVersion.getAppName().then(data => {
      this.appName = data;
    });
    this.appVersion.getPackageName().then(data => {
      this.packageName = data;
    });
    this.appVersion.getVersionCode().then(data => {
      this.versionCode = data;
    });
    this.appVersion.getVersionNumber().then(data => {
      this.versionNumber = data;
    });
  }

}
