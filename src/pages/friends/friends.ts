import { Component } from '@angular/core';
import { MyContacts } from '../../common/myContacts';
import { Contacts } from '@ionic-native/contacts';
import { MemberApi } from '../../providers/member.api';

@Component({
  selector: 'page-friends',
  templateUrl: 'friends.html',
  providers: [Contacts, MemberApi]
})
export class FriendsPage {
  friendContacts: any;

  constructor(public contacts: Contacts, public memberApi: MemberApi) { }

  ionViewDidEnter() {
    let myContacts = new MyContacts(this.contacts, this.memberApi);
    this.friendContacts = myContacts.friendContacts;
  }
}
