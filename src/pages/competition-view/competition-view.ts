import { Component } from '@angular/core';
import { NavParams, NavController } from 'ionic-angular';
import { MatchApi } from '../../providers/match.api';
import { Util } from '../../common/util';

@Component({
    selector: 'page-competition-view',
    templateUrl: 'competition-view.html',
    providers: [MatchApi]
})
export class CompetitionViewPage {
    id: number;
    index: number;
    subIndex: number;
    gameType: number;
    players: any;
    player1 = {};
    player2 = {};
    roundResult: any;
    currentRound: any;
    subResults: any;
    currentSubRound: any;
    isTiebreak: boolean;
    tiebreak: any;
    constructor(
        public navParams: NavParams,
        public navCtrl: NavController,
        public matchApi: MatchApi
    ) {
        this.id = navParams.get('id');
        this.index = navParams.get('index');
        this.subIndex = navParams.get('subIndex');
        this.isTiebreak = navParams.get('tiebreak') || false;
    }

    ionViewDidEnter() {
        this.matchApi.get(this.id).then(data => {
            this.gameType = data.game_type;
            this.roundResult = JSON.parse(Util.HtmlDecode(data.round_result));
            this.players = JSON.parse(Util.HtmlDecode(data.game_result));
            this.player1 = this.players[0];
            this.player2 = this.players[1];
            this.currentRound = this.roundResult[this.index];
            if (this.gameType == 3) {
                if (this.isTiebreak) {
                    this.tiebreak = this.currentRound.tiebreak;
                }
                else {
                    this.subResults = this.currentRound.sub;
                    this.currentSubRound = this.subResults[this.subIndex];
                }
            }
        })
    }

    endViewCompetition() {
        this.navCtrl.popToRoot();
    }
}
