import { Component } from '@angular/core';
import { NavParams, NavController, AlertController } from 'ionic-angular';
import { MatchApi } from '../../providers/match.api';
import { Util } from '../../common/util';

@Component({
    selector: 'page-competition-scoreboard',
    templateUrl: 'competition-scoreboard.html',
    providers: [MatchApi]
})
export class CompetitionScoreboardPage {
    id: number;
    index: number;
    subIndex: number;
    gameType: number;
    gameRound: number;
    players: any;
    player1: any;
    player2: any;
    roundResult: any;
    storage = window.localStorage;
    currentRound: any;
    isTiebreak: boolean;
    subResults: any;
    currentSubRound: any;
    tiebreak: any;

    constructor(
        public navParams: NavParams,
        public navCtrl: NavController,
        public matchApi: MatchApi,
        public alertCtrl: AlertController
    ) {
        this.id = navParams.get('id');
        this.index = navParams.get('index');
        this.subIndex = navParams.get('subIndex');
        this.isTiebreak = navParams.get('tiebreak') || false;
    }

    ionViewDidEnter() {
        this.matchApi.get(this.id).then(data => {
            this.gameType = data.game_type;
            this.gameRound = data.game_round;
            this.roundResult = data.round_result ? JSON.parse(Util.HtmlDecode(data.round_result)) : [];
            this.players = JSON.parse(Util.HtmlDecode(data.game_result));
            this.player1 = this.players[0];
            this.player2 = this.players[1];
            this.player1.score = this.player1.score || 0;
            this.player2.score = this.player2.score || 0;
            if (this.gameType != 3) {
                if (!this.roundResult.length) {
                    for (let r = 1; r <= this.gameRound; r++) {
                        let roundDetail = { id: r, score1: 0, score2: 0, status: 0 };
                        this.roundResult.push(roundDetail);
                    }
                }

                let tempResult = this.storage.getItem(this.id + "" + this.index);
                if (tempResult) {
                    this.roundResult[this.index] = tempResult;
                }

                this.currentRound = this.roundResult[this.index];
            }
            else {
                if (this.isTiebreak) {
                    this.currentRound = this.roundResult[this.index];
                    this.currentRound.tiebreak = {
                        score1: 0, score2: 0, aces1: 0, aces2: 0, doubles1: 0, doubles2: 0,
                        first1: 0, first2: 0, second1: 0, second2: 0, winner1: 0, winner2: 0, ue1: 0, ue2: 0
                    };

                    let tempTiebreak = this.storage.getItem(this.id + "" + this.index + "tiebreak");
                    if (tempTiebreak) {
                        this.currentRound.tiebreak = tempTiebreak;
                    }
                    this.tiebreak = this.currentRound.tiebreak;
                }
                else {
                    if (!this.roundResult.length) {
                        for (let r = 1; r <= this.gameRound; r++) {
                            let roundDetail = { id: r, score1: 0, score2: 0, status: 0, sub: [] };
                            for (let i = 1; i <= 12; i++) {
                                let subRoundDetail = {
                                    id: i, status: 0, score1: 0, score2: 0, aces1: 0, aces2: 0, doubles1: 0, doubles2: 0,
                                    first1: 0, first2: 0, second1: 0, second2: 0, winner1: 0, winner2: 0, ue1: 0, ue2: 0
                                };
                                roundDetail.sub.push(subRoundDetail);
                            }
                            this.roundResult.push(roundDetail);
                        }
                    }
                    this.currentRound = this.roundResult[this.index];
                    this.subResults = this.currentRound.sub;

                    let tempResult = this.storage.getItem(this.id + "" + this.index + "" + this.subIndex);
                    if (tempResult) {
                        this.subResults[this.subIndex] = JSON.parse(tempResult);
                    }
                    this.currentSubRound = this.subResults[this.subIndex];
                }
            }
        })
    }

    add(key, index) {
        if (this.gameType != 3) {
            if (this.currentRound[key + index]) {
                this.currentRound[key + index]++;
            }
            else {
                this.currentRound[key + index] = 1;
            }
        }
        else {
            if (this.isTiebreak) {
                if (this.currentRound.tiebreak[key + index]) {
                    this.currentRound.tiebreak[key + index]++;
                }
                else {
                    this.currentRound.tiebreak[key + index] = 1;
                }
            }
            else {
                if (this.currentSubRound[key + index]) {
                    this.currentSubRound[key + index]++;
                }
                else {
                    this.currentSubRound[key + index] = 1;
                }

            }
        }
    }

    minus(key, index) {
        if (this.gameType != 3) {
            if (this.currentRound[key + index] > 0) {
                this.currentRound[key + index] -= 1;
            }
        }
        else {
            if (this.isTiebreak) {
                if (this.currentRound.tiebreak[key + index] > 0) {
                    this.currentRound.tiebreak[key + index] -= 1;
                }
            }
            else {
                if (this.currentSubRound[key + index] > 0) {
                    this.currentSubRound[key + index] -= 1;
                }

            }
        }
    }

    saveTempResult() {
        if (this.gameType != 3) {
            this.storage.setItem(this.id + "" + this.index, JSON.stringify(this.currentRound));
        }
        else {
            if (this.isTiebreak) {
                this.storage.setItem(this.id + "" + this.index + "tiebreak", JSON.stringify(this.tiebreak));
            }
            else {
                this.storage.setItem(this.id + "" + this.index + "" + this.subIndex, JSON.stringify(this.currentSubRound));
            }
        }
    }

    endRound() {
        if (this.gameType != 3) {
            this.storage.removeItem(this.id + "" + this.index);
            this.currentRound.status = 1;
            if (this.currentRound.score1 > this.currentRound.score2) {
                this.player1.score++;
            }
            else {
                this.player2.score++;
            }
            let postData = { id: this.id, game_result: JSON.stringify(this.players), round_result: JSON.stringify(this.roundResult) };
            this.matchApi.updateresult(postData).then(response => {
                if (response.code == 0) {
                    this.navCtrl.pop();
                }
            });
        }
        else {
            if (this.isTiebreak) {
                this.storage.removeItem(this.id + "" + this.index + "tiebreak");
                this.currentRound.score1 = this.tiebreak.score1;
                this.currentRound.score2 = this.tiebreak.score2;
                let postData = { id: this.id, game_result: JSON.stringify(this.players), round_result: JSON.stringify(this.roundResult) };
                this.matchApi.updateresult(postData).then(response => {
                    if (response.code == 0) {
                        this.navCtrl.pop();
                    }
                });
            }
            else {
                this.storage.removeItem(this.id + "" + this.index + "" + this.subIndex);
                this.currentSubRound.status = 1;
                if (this.currentSubRound.score1 > this.currentSubRound.score2) {
                    this.currentRound.score1++;
                }
                else {
                    this.currentRound.score2++;
                }
                let postData = { id: this.id, game_result: JSON.stringify(this.players), round_result: JSON.stringify(this.roundResult) };
                this.matchApi.updateresult(postData).then(response => {
                    if (response.code == 0) {
                        this.navCtrl.pop();
                    }
                });
            }
        }
    }

    enableEndRound() {
        let endScore = 0;
        if (!this.gameType) {
            return false;
        }
        switch (this.gameType.toString()) {
            case "1":
                endScore = 11;
                return (Math.abs(this.currentRound.score1 - this.currentRound.score2) >= 2 && Math.max(this.currentRound.score1, this.currentRound.score2) == endScore) ||
                    (Math.max(this.currentRound.score1, this.currentRound.score2) > endScore && Math.abs(this.currentRound.score1 - this.currentRound.score2) == 2)
            case "2":
                endScore = 21;
                return (Math.abs(this.currentRound.score1 - this.currentRound.score2) >= 2 && Math.max(this.currentRound.score1, this.currentRound.score2) == endScore) ||
                    (Math.max(this.currentRound.score1, this.currentRound.score2) > endScore && Math.max(this.currentRound.score1, this.currentRound.score2) < 30 && Math.abs(this.currentRound.score1 - this.currentRound.score2) == 2) ||
                    (Math.max(this.currentRound.score1, this.currentRound.score2) > endScore && Math.max(this.currentRound.score1, this.currentRound.score2) == 30 && Math.abs(this.currentRound.score1 - this.currentRound.score2) == 1)
            case "3":
                if (this.isTiebreak) {
                    return (Math.max(this.tiebreak.score1, this.tiebreak.score2) == 7 && Math.abs(this.tiebreak.score1 - this.tiebreak.score2) >= 2) || (Math.max(this.tiebreak.score1, this.tiebreak.score2) > 7 && Math.abs(this.tiebreak.score1 - this.tiebreak.score2) == 2)
                }
                else {
                    endScore = 4;
                    return (Math.abs(this.currentSubRound.score1 - this.currentSubRound.score2) >= 2 && Math.max(this.currentSubRound.score1, this.currentSubRound.score2) == endScore) ||
                        (Math.max(this.currentSubRound.score1, this.currentSubRound.score2) > endScore && Math.abs(this.currentSubRound.score1 - this.currentSubRound.score2) == 2)
                }
        }
    }
}
