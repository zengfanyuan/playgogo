import { Component } from '@angular/core';
import { ModalController, AlertController, NavController } from 'ionic-angular';
import { TranslateService, LangChangeEvent } from 'ng2-translate';
import { LoginPage } from '../login/login';
import { AboutUSPage } from '../about-us/about-us';
import { MemberApi } from '../../providers/member.api';
import { MePhotoPage } from '../me-photo/me-photo';
import { ApiConfig } from '../../app/api.config';

@Component({
  selector: 'page-me',
  templateUrl: 'me.html',
  providers: [MemberApi]
})
export class MePage {

  language: any;
  storage = window.localStorage;
  username: string;
  saveLabel: string;
  cancelLabel: string;
  messageLabel: string;
  nameLabel: string;
  phtoSource: string;

  constructor(public modalCtrl: ModalController, public alertCtrl: AlertController,
    public translate: TranslateService, public memberApi: MemberApi,
    public navCtrl: NavController) {
    this.username = this.storage.getItem("name") || this.storage.getItem("mobile");
    this.language = translate.getDefaultLang();

    translate.get("COMMON.SAVE").subscribe((data: string) => {
      this.saveLabel = data;
    });
    translate.get("COMMON.CANCEL").subscribe((data: string) => {
      this.cancelLabel = data;
    });
    translate.get("ME.MESSAGE").subscribe((data: string) => {
      this.messageLabel = data;
    });
    translate.get("ME.NAME").subscribe((data: string) => {
      this.nameLabel = data;
    });

    translate.onLangChange
      .subscribe((event: LangChangeEvent) => {
        translate.get("COMMON.SAVE").subscribe((data: string) => {
          this.saveLabel = data;
        });
        translate.get("COMMON.CANCEL").subscribe((data: string) => {
          this.cancelLabel = data;
        });
        translate.get("ME.MESSAGE").subscribe((data: string) => {
          this.messageLabel = data;
        });
        translate.get("ME.NAME").subscribe((data: string) => {
          this.nameLabel = data;
        });
      });
  }

  ionViewDidEnter() {
    this.memberApi.info(0).then(data => {
      if (data.photo) {
        this.phtoSource = ApiConfig.getUploadPath() + "professor/" + data.photo;
      }
      else {
        this.phtoSource = "assets/img/user.jpg";
      }
    });
  }

  modifyName() {
    let prompt = this.alertCtrl.create({
      title: this.messageLabel,
      inputs: [
        {
          placeholder: this.nameLabel
        },
      ],
      buttons: [
        {
          text: this.cancelLabel
        },
        {
          text: this.saveLabel,
          handler: data => {
            if (data[0]) {
              let newName = data[0];
              let postData = { name: newName };
              this.memberApi.updateinfo(postData).then(response => {
                if (response.code == 0) {
                  this.username = newName;
                  this.storage.setItem("name", this.username);
                }
              });
            }
          }
        }
      ]
    });
    prompt.present();
  }

  modifyPhoto() {
    this.navCtrl.push(MePhotoPage);
  }

  setLanguage() {
    this.translate.use(this.language);
    this.storage.setItem("lang", this.language);
  }

  aboutUs() {
    this.navCtrl.push(AboutUSPage);
  }

  logOut() {
    this.storage.clear();
    this.navCtrl.setRoot(LoginPage);
    let modal = this.modalCtrl.create(LoginPage);
    modal.present();
  }

}
