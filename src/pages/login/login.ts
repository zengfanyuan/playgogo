import { Component } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { MemberApi } from '../../providers/member.api';
import { ApiConfig } from '../../app/api.config';
import { Util } from '../../common/util';
import { TabsPage } from '../tabs/tabs';

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  providers: [MemberApi]
})
export class LoginPage {
  intervalNumber: number = 0;
  interval: any;
  sendVerifyCodeText: string;
  getCodeText: string;
  getSecondText: string;
  username: any;
  password: any;
  storage = window.localStorage;

  constructor(public navCtrl: NavController, public alertCtrl: AlertController, public memberApi: MemberApi) { }

  sendCode() {
    this.intervalNumber = 60;
    this.interval = window.setInterval(() => {
      this.intervalNumber--;
      if (this.intervalNumber <= 0) {
        window.clearInterval(this.interval);
      }
    }, 1000);
  }

  validateSendCode() {
    if (this.intervalNumber <= 0 && Util.IsMobile(this.username)) {
      return false;
    }
    return true;
  }

  validateLogin() {
    if (this.password && this.password.length == 6) {
      return false;
    }
    return true;
  }

  login() {
    let postData = { mobile: this.username, verifycode: this.password };
    this.memberApi.login(postData).then(data => {
      if (data.code == 0) {
        ApiConfig.SetToken(data.return.token, data.return.id);
        this.storage.setItem("token", data.return.token);
        this.storage.setItem("id", data.return.id);
        this.memberApi.info(null).then(data => {
          data.mobile && this.storage.setItem("mobile", data.mobile);
          data.name && this.storage.setItem("name", data.name);
        });
        this.navCtrl.push(TabsPage);
        this.navCtrl.setRoot(TabsPage);
      } else {
        let alert = this.alertCtrl.create({
          title: data.result,
          buttons: ['OK']
        });
        alert.present();
      }
    });
  }
}
