import { Component } from '@angular/core';
import { NavParams, NavController } from 'ionic-angular';
import { MatchApi } from '../../providers/match.api';
import { Util } from '../../common/util';
import { CompetitionScoreboardPage } from '../competition-scoreboard/competition-scoreboard';
import { CompetitionViewPage } from '../competition-view/competition-view';
import { CompetitionSubRoundPage } from '../competition-sub-round/competition-sub-round';
import * as moment from 'moment';


@Component({
    selector: 'page-competition-round',
    templateUrl: 'competition-round.html',
    providers: [MatchApi]
})
export class CompetitionRoundPage {
    id: number;
    name: string;
    gameType: number;
    gameRound: number;
    gameStatus: number;
    player1: any;
    player2: any;
    player1Score: number;
    player2Score: number;
    roundResult: any;
    constructor(
        public navParams: NavParams,
        public navCtrl: NavController,
        public matchApi: MatchApi
    ) {
        this.id = navParams.get('id');
    }

    ionViewDidEnter() {
        this.matchApi.get(this.id).then(data => {
            this.name = data.name;
            this.gameType = data.game_type;
            this.gameRound = data.game_round;
            this.gameStatus = data.game_status;
            this.roundResult = data.round_result ? JSON.parse(Util.HtmlDecode(data.round_result)) : [];
            let players = JSON.parse(Util.HtmlDecode(data.game_result));
            this.player1 = players[0];
            this.player2 = players[1];
            if (!this.roundResult.length) {
                for (let r = 1; r <= this.gameRound; r++) {
                    let roundDetail = { id: r, score1: 0, score2: 0, status: 0 };
                    this.roundResult.push(roundDetail);
                }
            }

            this.player1.score = this.player1.score || 0;
            this.player2.score = this.player2.score || 0;
        })
    }

    clickRound(index) {
        if (this.gameStatus == 1 && this.roundResult[index].status == 0) {
            return this.startRound(index);
        }
        else if (this.roundResult[index].status == 1) {
            return this.viewRound(index);
        }
    }

    startRound(index) {
        if (index > 0 && this.roundResult[index - 1].status == 0) {
            return;
        }
        else {
            this.navCtrl.push(this.gameType != 3 ? CompetitionScoreboardPage : CompetitionSubRoundPage, { id: this.id, index: index });
        }
    }

    viewRound(index) {
        if (this.roundResult[index].status == 1) {
            this.navCtrl.push(this.gameType != 3 ? CompetitionViewPage : CompetitionSubRoundPage, { id: this.id, index: index });
        }
    }

    validateEndCompetition() {
        let endScore = 0;
        if (!this.gameRound) {
            return true;
        }
        switch (this.gameRound.toString()) {
            case "3":
                endScore = 2;
                break;
            case "5":
                endScore = 3;
                break;
            case "7":
                endScore = 4;
                break;
        }
        return this.player1.score < endScore && this.player2.score < endScore;
    }

    endCompetition() {
        let today = moment().format("YYYY-MM-DD");
        let postDate = { id: this.id, end_date: today };
        this.matchApi.setfinished(postDate).then(response => {
            if (response.code == 0) {
                this.navCtrl.pop();
            }
        });
    }
}
