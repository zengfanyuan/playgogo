import { Component } from '@angular/core';
import { AppUpdate } from '@ionic-native/app-update';
import { MePage } from '../me/me';
import { CompetitionPage } from '../competition/competition';
import { FriendsPage } from '../friends/friends';

@Component({
  templateUrl: 'tabs.html',
  providers: [AppUpdate]
})
export class TabsPage {

  // this tells the tabs component which Pages
  // should be each tab's root Page
  tab1Root: any = CompetitionPage;
  tab2Root: any = FriendsPage;
  tab3Root: any = MePage;

  constructor(private appUpdate: AppUpdate) {
    const updateUrl = 'http://sz-g8ysw72.morningstar.com/update.xml';
    this.appUpdate.checkAppUpdate(updateUrl);
  }

}
