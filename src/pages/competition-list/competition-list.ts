import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CompetitionRoundPage } from '../competition-round/competition-round';
import { MatchApi } from '../../providers/match.api';
import { Util } from '../../common/util';

@Component({
    selector: 'competition-list',
    templateUrl: 'competition-list.html',
    providers: [MatchApi]
})
export class CompetitionListPage {
    status: number;
    competitions = [];
    constructor(
        public navCtrl: NavController,
        public navParams: NavParams,
        public matchApi: MatchApi
    ) {
        this.status = navParams.get('status');
    }

    ionViewDidEnter() {
        let postData = { orderby: "create_date asc", game_status: this.status };
        this.matchApi.list(postData).then(data => {
            this.competitions = data;
            for (let competition of this.competitions) {
                competition.game_result = JSON.parse(Util.HtmlDecode(competition.game_result));
            }
        })
    }

    formatDate(date: string) {
        return Util.FormatDate(date);
    }

    clickCompetition(competition) {
        this.navCtrl.push(CompetitionRoundPage, { id: competition.id });
    }

    removeCompetition(competition) {
        let postData = { id: competition.id };
        this.matchApi.setremoved(postData).then(data => {
            if (data.code == 0) {
                this.ionViewDidEnter();
            }
        });
    }
}