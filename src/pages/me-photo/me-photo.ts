import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { MemberApi } from '../../providers/member.api';
import { ApiConfig } from '../../app/api.config';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { Transfer, FileUploadOptions, TransferObject } from '@ionic-native/transfer';

@Component({
  selector: 'page-me-photo',
  templateUrl: 'me-photo.html',
  providers: [MemberApi, Camera, Transfer]
})
export class MePhotoPage {
  fileTransfer: TransferObject;
  fileOptions: FileUploadOptions = {
    fileKey: 'photo'
  };

  constructor(public memberApi: MemberApi, public camera: Camera,
    public transfer: Transfer, public navCtrl: NavController) {
    this.fileTransfer = transfer.create();
  }

  selectImage() {
    let options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.NATIVE_URI,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true,
      correctOrientation: true,
      targetWidth: 500,
      targetHeight: 500
    }

    this.camera.getPicture(options).then(imagePath => {
      this.fileTransfer.upload(imagePath, ApiConfig.getFileUploadAPI(), this.fileOptions).then(data => {
        if (data.response) {
          let dataArray = data.response.split('|');
          if (dataArray[0] == "success") {
            let fileName = dataArray[2];
            let postData = { file_name: fileName };
            this.memberApi.updatephoto(postData).then(data => {
              if (data.code == 0) { }
            });
          }
        }
      });
      this.navCtrl.popToRoot();
    });
  }

  takePhoto() {
    let options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.NATIVE_URI,
      sourceType: this.camera.PictureSourceType.CAMERA,
      encodingType: this.camera.EncodingType.JPEG,
      allowEdit: true,
      correctOrientation: true,
      targetWidth: 500,
      targetHeight: 500
    }

    this.camera.getPicture(options).then(imagePath => {
      this.fileTransfer.upload(imagePath, ApiConfig.getFileUploadAPI(), this.fileOptions).then(data => {
        if (data.response) {
          let dataArray = data.response.split('|');
          if (dataArray[0] == "success") {
            let fileName = dataArray[2];
            let postData = { file_name: fileName };
            this.memberApi.updatephoto(postData).then(data => {
              if (data.code == 0) { }
            });
          }
        }
      });
      this.navCtrl.popToRoot();
    });
  }

}
