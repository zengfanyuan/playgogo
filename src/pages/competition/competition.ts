import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { NewCompetitionPage } from '../competition-new/competition-new';
import { CompetitionListPage } from '../competition-list/competition-list';
import { CompetitionRoundPage } from '../competition-round/competition-round';
import { MatchApi } from '../../providers/match.api';
import { Util } from '../../common/util';

@Component({
  selector: 'page-competition',
  templateUrl: 'competition.html',
  providers: [MatchApi]
})
export class CompetitionPage {
  competitionsNotFinished = [];
  competitionsFinished = [];
  slides = [
    {
      title: "Welcome to the Docs!",
      image: "assets/img/ica-slidebox-img-1.png",
    },
    {
      title: "What is Ionic?",
      image: "assets/img/ica-slidebox-img-2.png",
    },
    {
      title: "What is Ionic Cloud?",
      image: "assets/img/ica-slidebox-img-3.png",
    }
  ];


  constructor(public navCtrl: NavController, public matchApi: MatchApi) { }

  ionViewDidEnter() {
    let postData1 = { orderby: "create_date asc limit 0,5", game_status: 1 };
    this.matchApi.list(postData1).then(data => {
      this.competitionsNotFinished = data;
    })
    let postData2 = { orderby: "create_date asc limit 0,5", game_status: 2 };
    this.matchApi.list(postData2).then(data => {
      this.competitionsFinished = data;
      for (let competition of this.competitionsFinished) {
        competition.game_result = JSON.parse(Util.HtmlDecode(competition.game_result));
      }
    });
  }

  formatDate(date: string) {
    return Util.FormatDate(date);
  }

  newCompetition() {
    this.navCtrl.push(NewCompetitionPage);
  }

  getMoreNotFinished() {
    this.navCtrl.push(CompetitionListPage, { status: 1 });
  }

  getMoreFinished() {
    this.navCtrl.push(CompetitionListPage, { status: 2 });
  }

  clickCompetition(competition) {
    this.navCtrl.push(CompetitionRoundPage, { id: competition.id });
  }

  removeCompetition(competition) {
    let postData = { id: competition.id };
    this.matchApi.setremoved(postData).then(data => {
      if(data.code == 0)
      {
        this.ionViewDidEnter();
      }
    });
  }

}
