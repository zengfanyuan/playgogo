import { Component } from '@angular/core';
import { NavParams, NavController } from 'ionic-angular';
import { MatchApi } from '../../providers/match.api';
import { Util } from '../../common/util';
import { CompetitionScoreboardPage } from '../competition-scoreboard/competition-scoreboard';
import { CompetitionViewPage } from '../competition-view/competition-view';


@Component({
    selector: 'page-competition-sub-round',
    templateUrl: 'competition-sub-round.html',
    providers: [MatchApi]
})
export class CompetitionSubRoundPage {
    id: number;
    index: number;
    name: string;
    gameRound: number;
    gameStatus: number;
    players: any;
    player1: any;
    player2: any;
    player1Score: number;
    player2Score: number;
    roundResult: any;
    currentRound: any;
    subResults: any;
    constructor(
        public navParams: NavParams,
        public navCtrl: NavController,
        public matchApi: MatchApi
    ) {
        this.id = navParams.get('id');
        this.index = navParams.get('index');
    }

    ionViewDidEnter() {
        this.matchApi.get(this.id).then(data => {
            this.name = data.name;
            this.gameRound = data.game_round;
            this.gameStatus = data.game_status;
            this.roundResult = data.round_result ? JSON.parse(Util.HtmlDecode(data.round_result)) : [];
            this.players = JSON.parse(Util.HtmlDecode(data.game_result));
            this.player1 = this.players[0];
            this.player2 = this.players[1];
            this.player1.score = this.player1.score || 0;
            this.player2.score = this.player2.score || 0;
            if (!this.roundResult.length) {
                for (let r = 1; r <= this.gameRound; r++) {
                    let roundDetail = { id: r, score1: 0, score2: 0, status: 0, sub: [] };
                    for (let i = 1; i <= 12; i++) {
                        let subRoundDetail = { id: i, score1: 0, score2: 0, status: 0 };
                        roundDetail.sub.push(subRoundDetail);
                    }
                    this.roundResult.push(roundDetail);
                }
            }
            this.currentRound = this.roundResult[this.index];
            this.subResults = this.currentRound.sub;
        })
    }

    clickSubRound(index) {
        if (this.currentRound.status == 0 && this.subResults[index].status == 0 && !this.roundCanEnd()) {
            return this.startSubRound(index);
        }
        else if (this.subResults[index].status == 1) {
            return this.viewSubRound(index);
        }
    }

    startSubRound(index) {
        if (index > 0 && this.subResults[index - 1].status == 0) {
            return;
        }
        else {
            this.navCtrl.push(CompetitionScoreboardPage, { id: this.id, index: this.index, subIndex: index });
        }
    }

    viewSubRound(index) {
        if (this.subResults[index].status == 1) {
            this.navCtrl.push(CompetitionViewPage, { id: this.id, index: this.index, subIndex: index });
        }
    }

    clickTiebreak() {
        if (this.currentRound.tiebreak) {
            this.navCtrl.push(CompetitionViewPage, { id: this.id, index: this.index, tiebreak: true });
        }
        else {
            this.navCtrl.push(CompetitionScoreboardPage, { id: this.id, index: this.index, tiebreak: true });
        }
    }

    showTiebreak() {
        if (this.currentRound && this.currentRound.tiebreak) {
            return true;
        }

        if (this.subResults && this.subResults.length) {
            let score1 = 0;
            let score2 = 0;
            for (let s of this.subResults) {
                s.score1 > s.score2 && score1++;
                s.score1 < s.score2 && score2++;
            }
            return score1 == 6 && score2 == 6;
        }

        return false;
    }

    showSubRound(subresult) {
        if (this.currentRound) {
            if (this.roundCanEnd()) {
                if (subresult.status) {
                    return true;
                }
            }
            else {
                return true;
            }
        }
        return false;
    }

    roundCanEnd() {
        if (this.currentRound) {
            return (Math.max(this.currentRound.score1, this.currentRound.score2) == 6 && Math.abs(this.currentRound.score1 - this.currentRound.score2) >= 2) ||
                this.currentRound.tiebreak;
        }
        return false;
    }

    endRound() {
        this.currentRound.status = 1;
        if (this.currentRound.score1 > this.currentRound.score2) {
            this.player1.score++;
        }
        else {
            this.player2.score++;
        }
        let postData = { id: this.id, game_result: JSON.stringify(this.players), round_result: JSON.stringify(this.roundResult) };
        this.matchApi.updateresult(postData).then(response => {
            if (response.code == 0) {
                this.navCtrl.pop();
            }
        });
    }
}
